package me.codeleep.jsondiff.test.simple;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import me.codeleep.jsondiff.common.model.JsonCompareResult;
import me.codeleep.jsondiff.common.model.JsonComparedOption;
import me.codeleep.jsondiff.core.DefaultJsonDifference;
import org.testng.annotations.Test;

/**
 * @author: codeleep
 * @createTime: 2023/10/10 11:02
 * @description:
 */
public class TemporaryTest {


    @Test(description = "2023-10-08 反馈以下例子抛出 【key不允许出现非String类型】。已解决")
    public void  test() {
        String str = "{\"a\":\"a\",\"b\":\"b\",\"c\":[{\"aa\":\"aa\"},{\"bb\":\"bb\"},{\"cc\":\"cc\"}]}";
        String str2 = "{\"b\":\"b\",\"a\":\"a\",\"c\":[{\"bb\":\"bb\"},{\"aa\":\"aa\"},{\"cc\":\"cc\"}]}";

        JsonComparedOption jsonComparedOption = new JsonComparedOption().setIgnoreOrder(true);
        JsonCompareResult jsonCompareResult = new DefaultJsonDifference()
                .option(jsonComparedOption)
                .detectDiff(JSON.parseObject(str), JSON.parseObject(str2));
        System.out.println(JSON.toJSONString(jsonCompareResult));
    }

    @Test
    public void test4() {
        String array1 = "[{\"cooperativeBookTypeId\":2,\"officeName\":\"新增信息\",\"responsibleEditorName\":\"新增责任编辑姓名\",\"id\":71,\"typeQuantity\":\"10\"}]";
        String array2 = "[{\"cooperativeBookTypeId\":2,\"officeName\":\"新增信息\",\"responsibleEditorName\":\"新增责任编辑姓名\",\"id\":71,\"typeQuantity\":\"10\",\"responsiblePersonName\":\"编辑第一次，新增负责人姓名\"}]";

        JsonComparedOption jsonComparedOption = new JsonComparedOption().setIgnoreOrder(true);
        JsonCompareResult jsonCompareResult = new DefaultJsonDifference()
                .option(jsonComparedOption)
                .detectDiff(JSON.parseArray(array1), JSON.parseArray(array2));
        System.out.println(JSON.toJSONString(jsonCompareResult));


        String str1 = "{\"cooperativeBookTypeId\":2,\"officeName\":\"新增信息\",\"responsibleEditorName\":\"新增责任编辑姓名\",\"id\":71,\"typeQuantity\":\"10\"}";
        String str2 = "{\"cooperativeBookTypeId\":2,\"officeName\":\"新增信息\",\"responsibleEditorName\":\"新增责任编辑姓名\",\"id\":71,\"typeQuantity\":\"10\",\"responsiblePersonName\":\"编辑第一次，新增负责人姓名\"}";
        JsonCompareResult jsonCompareResult2 = new DefaultJsonDifference()
                .option(jsonComparedOption)
                .detectDiff(JSONObject.parseObject(str1), JSONObject.parseObject(str2));
        System.out.println(JSONObject.toJSONString(jsonCompareResult2));

    }

}
